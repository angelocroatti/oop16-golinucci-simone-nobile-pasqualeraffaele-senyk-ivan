package oop.raccoonhome.device;

/**
 * Device propriety for recognize the device.
 *
 */
public enum EDevice {
    /**
     * Represent a Shutter device.
     */
    SHUTTER, 
    /**
     * Represent a Door device.
     */
    DOOR,
    /**
     * Represent a Lamp device.
     */
    LAMP, 
    /**
     * Represent a left Lamp or kitchen Lamp device.
     */
    LAMP_L, 
    /**
     * Represent a right Lamp device.
     */
    LAMP_R, 
    /**
     * Represent a mirror Lamp device.
     */
    LAMP_MIRROR, 
    /**
     * Represent a TV Lamp device.
     */
    LAMP_TV;

}
