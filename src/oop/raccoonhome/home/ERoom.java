package oop.raccoonhome.home;

/**
 * 
 *
 */
public enum ERoom {
    /**
     * Represent a kitchen room.
     */
    CUCINA,
    /**
     * Represent a living room.
     */
    SALOTTO,
    /**
     * Represent a bed room.
     */
    CAMERADALETTO,
    /**
     * Represent a living room.
     */
    SOGGIORNO,
    /**
     * 
     */
    BAGNO,
    /**
     * 
     */
    CANTINA,
    /**
     * 
     */
    SOFFITTA,
    /**
     * 
     */
    MULTIUSO,

    /**
     * 
     */
    GARAGE;
}
